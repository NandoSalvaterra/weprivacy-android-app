package br.com.solucaoweprivacy.weprivacy.ui.detail;

import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import br.com.solucaoweprivacy.weprivacy.R;
import br.com.solucaoweprivacy.weprivacy.WePrivacyApp;
import br.com.solucaoweprivacy.weprivacy.databinding.ActivityAppDetailBinding;
import br.com.solucaoweprivacy.weprivacy.model.App;
import br.com.solucaoweprivacy.weprivacy.model.Goal;
import br.com.solucaoweprivacy.weprivacy.model.Resource;
import br.com.solucaoweprivacy.weprivacy.model.ResourceGoal;
import br.com.solucaoweprivacy.weprivacy.model.Reviewer;
import br.com.solucaoweprivacy.weprivacy.ui.ReviewItemDecoration;
import br.com.solucaoweprivacy.weprivacy.ui.adapter.ReviewAdapter;


public class AppDetailActivity extends AppCompatActivity implements AppDetailContract.View {

    private ActivityAppDetailBinding binding;
    private AppDetailContract.AppDetailPresentation presenter;
    private ReviewAdapter reviewAdapter;
    private App app;
    Set<Resource> resources = new HashSet<>();
    private List<Reviewer> reviews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_app_detail);
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);

        presenter = new AppDetailPresenter(WePrivacyApp.instance.getApiService(), this);
        showIntermittentProgress(true);
        app = getIntent().getParcelableExtra("app");
        binding.setApp(app);

        reviewAdapter = new ReviewAdapter();
        binding.comfortReviewsRecyclerView.setAdapter(reviewAdapter);
        binding.comfortReviewsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        binding.comfortReviewsRecyclerView.addItemDecoration(new ReviewItemDecoration(this));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(app.getName());

        presenter.loadMoreInfo(Integer.toString(app.getId()));
    }


    @Override
    public void showMoreInfo(List<ResourceGoal> resourceGoals) {
        for (ResourceGoal resourceGoal : resourceGoals) {
            Resource resource = resourceGoal.getResource();
            Goal goal = resourceGoal.getGoal();
            if (resources.contains(resource)) {
                for (Resource resource1 : resources) {
                    if (resource1.getId().equals(resource.getId())) {
                        resource1.getGoals().add(goal);
                    }
                }
            } else {
                resource.getGoals().add(goal);
                resources.add(resourceGoal.getResource());
            }
        }
        presenter.loadReviews(Integer.toString(app.getId()));

    }


    @Override
    public void showReviews(List<Reviewer> reviews) {
        this.reviews = reviews;
        reviewAdapter.setReviews(reviews);
        reviewAdapter.notifyDataSetChanged();

    }

    @Override
    public void showIntermittentProgress(boolean active) {
        binding.progressBar.setVisibility(active ? View.VISIBLE : View.GONE);
        binding.appContainer.setVisibility(active ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void showError() {

    }


    public void showGoalsForContacts(View view) {
        showDialogFor(1);
    }

    public void showGoalsForGPS(View view) {
        showDialogFor(2);
    }

    public void showGoalsForIP(View view) {
        showDialogFor(3);

    }

    public void showGoalsForCelular(View view) {
        showDialogFor(4);
    }


    public void showDialogFor(int id) {
        Resource resource = new Resource();
        String text = "";
        for (Resource resource1 : resources) {
            if (resource1.getId().equals(String.valueOf(id))) {
                resource = resource1;
            }
        }


        AlertDialog.Builder builder;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }

        for (Goal goal : resource.getGoals()) {
            text += goal.getName() + System.getProperty("line.separator") + "\n";
        }

        builder.setTitle(resource.getName())
                .setMessage(text)
                .setNegativeButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(resource.getIcon())
                .show();
    }

}
