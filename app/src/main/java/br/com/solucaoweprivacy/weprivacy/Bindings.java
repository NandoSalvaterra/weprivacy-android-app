package br.com.solucaoweprivacy.weprivacy;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by Luiz Fernando Salvaterra on 26/09/2017.
 */

public final class Bindings {

    @BindingAdapter({"imageUrl"})
    public static void imageUrl(ImageView view, String imageUrl) {
        Picasso.with(view.getContext()).load(imageUrl).into(view);
    }

    @BindingAdapter({"resourceUrl"})
    public static void resourceUrl(ImageView view, int resourceUrl) {
     Picasso.with(view.getContext()).load(resourceUrl).into(view);
    }

}
