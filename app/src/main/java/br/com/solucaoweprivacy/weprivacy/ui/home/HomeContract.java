package br.com.solucaoweprivacy.weprivacy.ui.home;

import java.util.List;

import br.com.solucaoweprivacy.weprivacy.model.App;

/**
 * Created by luizfernandosalvaterra on 27/02/2018.
 */

public interface HomeContract {

    interface View {
        void showHomeApps(List<App> apps);
        void showIntermittentProgress(boolean active);
        void showError();
    }

    interface HomePresentation {
        void loadHomeApps(String id);
        void refresh(String id);
        void onDestroy();
    }
}
