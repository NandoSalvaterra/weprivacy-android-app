package br.com.solucaoweprivacy.weprivacy.ui.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.text.Html;
import android.view.Menu;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import br.com.solucaoweprivacy.weprivacy.R;
import br.com.solucaoweprivacy.weprivacy.WePrivacyApp;
import br.com.solucaoweprivacy.weprivacy.databinding.ActivityHomeBinding;
import br.com.solucaoweprivacy.weprivacy.model.App;
import br.com.solucaoweprivacy.weprivacy.ui.AppItemDecoration;
import br.com.solucaoweprivacy.weprivacy.ui.detail.AppDetailActivity;
import br.com.solucaoweprivacy.weprivacy.ui.activity.SurveyActivity;
import br.com.solucaoweprivacy.weprivacy.ui.adapter.AppAdapter;

public class HomeActivity extends AppCompatActivity implements HomeContract.View, AppAdapter.OnAppClickListener {

    private ActivityHomeBinding binding;
    private HomePresenter presenter;
    private AppAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("");

        presenter = new HomePresenter(WePrivacyApp.instance.getApiService(), this);

        GridLayoutManager manager = new GridLayoutManager(this, 2);
        binding.appRecyclerView.setLayoutManager(manager);
        binding.appRecyclerView.addItemDecoration(new AppItemDecoration(this));
        adapter = new AppAdapter(this);
        binding.appRecyclerView.setAdapter(adapter);

        presenter.loadHomeApps("1"); //1 For active Apps

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onAppClick(App app) {
        Intent intent = new Intent(this, AppDetailActivity.class);
        intent.putExtra("app", app);
        startActivity(intent);

    }

    @Override
    public void showHomeApps(List<App> apps) {
        adapter.setApps(apps);
        adapter.notifyDataSetChanged();

    }

    @Override
    public void showIntermittentProgress(boolean active) {
        binding.progressBar.setVisibility(active ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError() {

    }

    public void showSurveyDialog(View view) {

        AlertDialog.Builder builder;

        List<ApplicationInfo> list = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);

        List<String> appNames = new ArrayList<>();

        for (ApplicationInfo info : list) {

            if ((info.flags & ApplicationInfo.FLAG_SYSTEM) == 0) {
                //user application
                appNames.add((String) info.loadLabel(getPackageManager()));
            }
        }
        Random randomGenerator = new Random();
        int index = randomGenerator.nextInt(appNames.size());
        final String randomAppName = appNames.get(index);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Light_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }

        String text = String.format(getResources().getString(R.string.start_survey_popup_message), randomAppName);
        CharSequence styledText = Html.fromHtml(text);

        builder.setTitle(getString(R.string.start_survey_popup_title))
                .setMessage(styledText)
                .setPositiveButton("Iniciar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                   Intent intent = new Intent(HomeActivity.this, SurveyActivity.class);
                        intent.putExtra("appName", randomAppName);
                        startActivity(intent);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(R.drawable.ic_survey_alert)
                .show();
    }
}
