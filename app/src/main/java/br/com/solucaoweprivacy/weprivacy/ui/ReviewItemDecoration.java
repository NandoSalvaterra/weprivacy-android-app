package br.com.solucaoweprivacy.weprivacy.ui;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.solucaoweprivacy.weprivacy.R;

public class ReviewItemDecoration extends RecyclerView.ItemDecoration {

    private final int margin;

    public ReviewItemDecoration(Context context) {
        this.margin = context.getResources()
                .getDimensionPixelSize(R.dimen.space_card_top);


    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        LinearLayoutManager linearLayoutManager = (LinearLayoutManager) parent.getLayoutManager();

        final int pos = linearLayoutManager.getPosition(view);
        final boolean lastItem = pos + 1 == parent.getAdapter().getItemCount();

        if(pos == 0) {
            outRect.top = margin;
        } else {
            outRect.top = margin/2;
        }

        if (lastItem) {
            outRect.bottom = margin;
        }  else {
            outRect.bottom = margin / 2;
        }

        outRect.left = margin;
        outRect.right = margin;
    }
}