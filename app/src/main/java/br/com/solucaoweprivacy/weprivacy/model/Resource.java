package br.com.solucaoweprivacy.weprivacy.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import br.com.solucaoweprivacy.weprivacy.R;

/**
 * Created by luizfernandosalvaterra on 26/03/2018.
 */

public class Resource implements Parcelable {


    @SerializedName("idRecurso")
    private String id;

    @SerializedName("recurso")
    private String name;


    private List<Goal> goals = new ArrayList<>();

    protected Resource(Parcel in) {
        id = in.readString();
        name = in.readString();
        goals = in.createTypedArrayList(Goal.CREATOR);
    }

    public Resource() {}

    public static final Creator<Resource> CREATOR = new Creator<Resource>() {
        @Override
        public Resource createFromParcel(Parcel in) {
            return new Resource(in);
        }

        @Override
        public Resource[] newArray(int size) {
            return new Resource[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIcon() {
      if (getId().equals("1")) {
        return R.drawable.ic_contacts;
      } else if (getId().equals("2")) {
        return R.drawable.ic_gps;
      } else if (getId().equals("3")){
          return R.drawable.ic_wifi;
      } else if (getId().equals("4")){
          return R.drawable.ic_phone;
      } else {
          return 1;
      }
    }



    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeTypedList(goals);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        final Resource resource = (Resource) obj;

        return this.id.equals(resource.getId());
    }

    @Override
    public int hashCode() {
        return Integer.valueOf(id);
    }
}
