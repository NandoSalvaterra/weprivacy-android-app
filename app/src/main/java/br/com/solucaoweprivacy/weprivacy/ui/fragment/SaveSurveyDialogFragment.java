package br.com.solucaoweprivacy.weprivacy.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.solucaoweprivacy.weprivacy.R;
import br.com.solucaoweprivacy.weprivacy.databinding.FragmentSaveSurveyDialogBinding;


/**
 * Created by luizfernandosalvaterra on 24/03/2018.
 */

public class SaveSurveyDialogFragment extends DialogFragment {

   private FragmentSaveSurveyDialogBinding binding;


    public static SaveSurveyDialogFragment newInstance() {
        SaveSurveyDialogFragment fragment = new SaveSurveyDialogFragment();
        Bundle args = new Bundle();
       // args.putString("appName", appName);
      //  fragment.setArguments(args);

        return fragment;
    }

   @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSaveSurveyDialogBinding.inflate(inflater, container, false);
        binding.titleTextView.setText(R.string.sending_survey);
        binding.messageTextView.setVisibility(View.INVISIBLE);
        binding.closeButton.setVisibility(View.GONE);
        binding.progressBar.setVisibility(View.VISIBLE);
        return binding.getRoot();
    }
}
