package br.com.solucaoweprivacy.weprivacy.client;

import java.util.List;

import br.com.solucaoweprivacy.weprivacy.model.App;
import br.com.solucaoweprivacy.weprivacy.model.ResourceGoal;
import br.com.solucaoweprivacy.weprivacy.model.ResourceReview;
import br.com.solucaoweprivacy.weprivacy.model.Review;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Luiz Fernando Salvaterra on 26/02/2018.
 */

public interface WePrivacyService {

    @GET("aplicativo/processado/{id}")
    Call<List<App>> getHomeApps(@Path("id") String id);

    @GET("aplicativorecursofinalidade/aplicativo/{id}")
    Call<List<ResourceGoal>>getGoals(@Path("id") String appId);

    @GET("analiseproposito/aplicativo/{id}")
    Call<List<Review>>getComfortReviews(@Path("id") String appId);

    @GET("analiseexpectativa/aplicativo/{id}")
    Call<List<ResourceReview>>getResourceReviews(@Path("id") String appId);

}
