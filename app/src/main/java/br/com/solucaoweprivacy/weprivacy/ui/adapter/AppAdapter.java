package br.com.solucaoweprivacy.weprivacy.ui.adapter;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.solucaoweprivacy.weprivacy.databinding.AppItemViewBinding;
import br.com.solucaoweprivacy.weprivacy.model.App;

public class AppAdapter extends RecyclerView.Adapter<AppAdapter.AppViewHolder> {

    private List<App> apps = new ArrayList<>();
    private OnAppClickListener listener;

    public AppAdapter(OnAppClickListener listener) {
        this.listener = listener;
    }

    @Override
    public AppViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new AppViewHolder(AppItemViewBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(AppViewHolder holder, int position) {
        final App app = apps.get(position);
        holder.binding.setApp(app);
        holder.binding.executePendingBindings();
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onAppClick(app);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return apps.size();
    }

    public void setApps(List<App> apps) {
        this.apps = apps;
    }

    class AppViewHolder extends RecyclerView.ViewHolder {
        final AppItemViewBinding binding;

        AppViewHolder(AppItemViewBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public interface OnAppClickListener {
        void onAppClick(App app);
    }
}


