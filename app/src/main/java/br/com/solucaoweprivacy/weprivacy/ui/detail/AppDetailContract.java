package br.com.solucaoweprivacy.weprivacy.ui.detail;

import java.util.List;

import br.com.solucaoweprivacy.weprivacy.model.ResourceGoal;
import br.com.solucaoweprivacy.weprivacy.model.Review;
import br.com.solucaoweprivacy.weprivacy.model.Reviewer;

public interface AppDetailContract {

    interface View {
        void showMoreInfo(List<ResourceGoal> resourceGoals);
        void showReviews(List<Reviewer> reviews);
        void showIntermittentProgress(boolean active);
        void showError();
    }

    interface AppDetailPresentation {
        void loadMoreInfo(String appId);
        void loadReviews(String appId);
        void refresh(String id);
        void onDestroy();
    }


}
