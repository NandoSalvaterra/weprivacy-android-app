package br.com.solucaoweprivacy.weprivacy.ui.activity;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.CompoundButton;

import br.com.solucaoweprivacy.weprivacy.R;
import br.com.solucaoweprivacy.weprivacy.databinding.ActivitySurveyBinding;
import br.com.solucaoweprivacy.weprivacy.ui.fragment.SurveyDialogFragment;

public class SurveyActivity extends AppCompatActivity {

    private ActivitySurveyBinding binding ;
    private String appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_survey);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle("");
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appName = (String) getIntent().getSerializableExtra("appName");
        String text = String.format(getResources().getString(R.string.survey_question), appName);
        CharSequence styledText = Html.fromHtml(text);
        binding.questionTextView.setText(styledText);

        String text2 = String.format(getResources().getString(R.string.survey_question_1), appName);
        CharSequence styledText2 = Html.fromHtml(text2);
        binding.surveyTextView.setText(styledText2);

        binding.checkBoxListaContatos.setOnCheckedChangeListener(onCheckBoxChangeListener());
        binding.checkBoxLocalizacaoAproximada.setOnCheckedChangeListener(onCheckBoxChangeListener());
        binding.checkBoxEnderecoIP.setOnCheckedChangeListener(onCheckBoxChangeListener());
        binding.checkBoxIdentificacaoCelular.setOnCheckedChangeListener(onCheckBoxChangeListener());

    }


    private CompoundButton.OnCheckedChangeListener onCheckBoxChangeListener() {

        return new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        ft.remove(prev);
                    }
                    ft.addToBackStack(null);

                    // Create and show the dialog.
                    SurveyDialogFragment newFragment = SurveyDialogFragment.newInstance(appName);
                    newFragment.show(ft, "dialog");

                }
            }
        };
    }

    public void next(View view) {
    //Save here, do validations here

        Intent intent = new Intent(SurveyActivity.this, ExpectationActivity.class);
        intent.putExtra("appName", appName);
        startActivity(intent);
    }


}
