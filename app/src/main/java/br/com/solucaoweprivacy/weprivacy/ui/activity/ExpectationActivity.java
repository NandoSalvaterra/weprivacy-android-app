package br.com.solucaoweprivacy.weprivacy.ui.activity;

import android.databinding.DataBindingUtil;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;

import br.com.solucaoweprivacy.weprivacy.R;
import br.com.solucaoweprivacy.weprivacy.databinding.ActivityExpectationBinding;
import br.com.solucaoweprivacy.weprivacy.ui.fragment.SaveSurveyDialogFragment;
import br.com.solucaoweprivacy.weprivacy.ui.fragment.SurveyDialogFragment;

public class ExpectationActivity extends AppCompatActivity {

    private ActivityExpectationBinding binding;
    private String appName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_expectation);
        Toolbar toolbar = binding.toolbar;
        setSupportActionBar(toolbar);
        appName = getIntent().getStringExtra("appName");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(appName);

        String question = getString(R.string.expectation_question, appName);
        CharSequence formattedQuestion = Html.fromHtml(question);
        binding.questionTextView.setText(formattedQuestion);

    }

    public void save(View view) {

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);

        // Create and show the dialog.
        SaveSurveyDialogFragment newFragment = SaveSurveyDialogFragment.newInstance();
        newFragment.show(ft, "dialog");

    }
}
