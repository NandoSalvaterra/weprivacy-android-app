package br.com.solucaoweprivacy.weprivacy.ui.detail;

import java.util.ArrayList;
import java.util.List;

import br.com.solucaoweprivacy.weprivacy.client.WePrivacyService;
import br.com.solucaoweprivacy.weprivacy.model.ResourceGoal;
import br.com.solucaoweprivacy.weprivacy.model.ResourceReview;
import br.com.solucaoweprivacy.weprivacy.model.Review;
import br.com.solucaoweprivacy.weprivacy.model.Reviewer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppDetailPresenter implements AppDetailContract.AppDetailPresentation {

    private WePrivacyService apiService;
    private AppDetailContract.View appDetailView;

    public AppDetailPresenter(WePrivacyService apiService, AppDetailContract.View appDetailView) {
        this.apiService = apiService;
        this.appDetailView = appDetailView;
    }


    @Override
    public void loadMoreInfo(String id) {

        appDetailView.showIntermittentProgress(true);

        apiService.getGoals(id).enqueue(new Callback<List<ResourceGoal>>() {
            @Override
            public void onResponse(Call<List<ResourceGoal>> call, Response<List<ResourceGoal>> response) {
                appDetailView.showMoreInfo(response.body());
            }

            @Override
            public void onFailure(Call<List<ResourceGoal>> call, Throwable t) {
                appDetailView.showIntermittentProgress(false);
                appDetailView.showError();

            }
        });

    }

    @Override
    public void loadReviews(final String appId) {
        final List<Reviewer> reviews = new ArrayList<>();
        apiService.getComfortReviews(appId).enqueue(new Callback<List<Review>>() {
            @Override
            public void onResponse(Call<List<Review>> call, Response<List<Review>> response) {
                List<Review> comfortReviews = response.body();

                for (Review review : comfortReviews) {
                    review.setFinalDescription(review.getAvarage(), review.getDescription());
                }

                reviews.addAll(comfortReviews);

                apiService.getResourceReviews(appId).enqueue(new Callback<List<ResourceReview>>() {
                    @Override
                    public void onResponse(Call<List<ResourceReview>> call, Response<List<ResourceReview>> response) {
                        List<ResourceReview> resourceReviews = response.body();
                        for (ResourceReview review : resourceReviews) {
                            review.setFinalDescription(review.getAverage(), review.getDescription());
                        }
                        reviews.addAll(resourceReviews);

                        appDetailView.showIntermittentProgress(false);
                        appDetailView.showReviews(reviews);
                    }

                    @Override
                    public void onFailure(Call<List<ResourceReview>> call, Throwable t) {
                        appDetailView.showIntermittentProgress(false);
                        appDetailView.showError();
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Review>> call, Throwable t) {
                appDetailView.showIntermittentProgress(false);
                appDetailView.showError();
            }
        });
    }

    @Override
    public void refresh(String id) {

    }

    @Override
    public void onDestroy() {
        appDetailView = null;
    }
}
