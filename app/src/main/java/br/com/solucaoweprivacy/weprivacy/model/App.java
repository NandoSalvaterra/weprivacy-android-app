package br.com.solucaoweprivacy.weprivacy.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Luiz Fernando Salvaterra on 26/09/2017.
 */

public class App implements Parcelable {

    @SerializedName("idAplicativo")
    private int id;

    @SerializedName("aplicativo")
    private String name;

    @SerializedName("icone")
    private String icon;

    @SerializedName("logo")
    private String logo;

    @SerializedName("empresa")
    private String owner;

    @SerializedName("ranking")
    private float rating;

    public App(){}

    protected App(Parcel in) {
        id = in.readInt();
        name = in.readString();
        icon = in.readString();
        logo = in.readString();
        owner = in.readString();
        rating = in.readFloat();
    }

    public static final Creator<App> CREATOR = new Creator<App>() {
        @Override
        public App createFromParcel(Parcel in) {
            return new App(in);
        }

        @Override
        public App[] newArray(int size) {
            return new App[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(icon);
        parcel.writeString(logo);
        parcel.writeString(owner);
        parcel.writeFloat(rating);
    }
}
