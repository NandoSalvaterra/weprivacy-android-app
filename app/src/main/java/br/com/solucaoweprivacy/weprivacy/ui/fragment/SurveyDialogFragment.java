package br.com.solucaoweprivacy.weprivacy.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.com.solucaoweprivacy.weprivacy.R;
import br.com.solucaoweprivacy.weprivacy.databinding.FragmentSurveyDialogBinding;

/**
 * Created by luizfernandosalvaterra on 24/03/2018.
 */

public class SurveyDialogFragment extends DialogFragment {

    private FragmentSurveyDialogBinding binding;
    private String appName;

    public static SurveyDialogFragment newInstance(String appName) {
        SurveyDialogFragment fragment = new SurveyDialogFragment();
        Bundle args = new Bundle();
        args.putString("appName", appName);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        appName = getArguments().getString("appName");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = FragmentSurveyDialogBinding.inflate(inflater, container, false);
        String question = getString(R.string.survey_question_popup, appName);
        CharSequence questionFormatted = Html.fromHtml(question);

        binding.surveyQuestionTextview.setText(questionFormatted);

        String shareWithCompaniesOption = getString(R.string.share_data_with_other_companies, appName);
        CharSequence shareWithCompaniesOptionFormatted = Html.fromHtml(shareWithCompaniesOption);
        binding.checkBoxShareData.setText(shareWithCompaniesOptionFormatted);

        String noIdeaOption = getString(R.string.no_idea_message, appName);
        CharSequence noIdeaOptionFormatted = Html.fromHtml(noIdeaOption);
        binding.checkBoxNoIdea.setText(noIdeaOptionFormatted);

        return binding.getRoot();
    }

    public void save(View view) {

    }
}
