package br.com.solucaoweprivacy.weprivacy.ui.adapter;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.solucaoweprivacy.weprivacy.BR;
import br.com.solucaoweprivacy.weprivacy.databinding.ComfortReviewItemViewBinding;
import br.com.solucaoweprivacy.weprivacy.databinding.ResourceReviewItemViewBinding;
import br.com.solucaoweprivacy.weprivacy.model.ResourceReview;
import br.com.solucaoweprivacy.weprivacy.model.Review;
import br.com.solucaoweprivacy.weprivacy.model.Reviewer;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ReviewerViewHolder<?>> {

    private static final int RESOURCE_REVIEW_TYPE = 1;
    private static final int COMFORT_REVIEW_TYPE = 2;

    private List<Reviewer> reviews = new ArrayList<>();


    @Override
    public ReviewerViewHolder<?> onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case COMFORT_REVIEW_TYPE:
                return new ComfortReviewViewHolder(ComfortReviewItemViewBinding.inflate(inflater, parent, false));

            case RESOURCE_REVIEW_TYPE:
            return new ResourceReviewViewHolder(ResourceReviewItemViewBinding.inflate(inflater, parent,false ));

            default:
                throw new UnknownError("Unknown view holder to view type: " + viewType);
        }
    }

    @Override
    public void onBindViewHolder(final ReviewerViewHolder<?> holder, int position) {
        final Reviewer review = reviews.get(position);

        if (review instanceof  Review) {
            holder.binding.setVariable(BR.comfortReview, review);

        } else if (review instanceof ResourceReview) {
            holder.binding.setVariable(BR.resourceReview, review);

        }
    }

    @Override
    public int getItemCount() {
        return reviews.size();
    }

    @Override
    public int getItemViewType(int position) {
        Reviewer review = reviews.get(position);

        if(review instanceof Review) {
            return COMFORT_REVIEW_TYPE;
        } else if (review instanceof ResourceReview) {
            return  RESOURCE_REVIEW_TYPE;
        }

        throw new UnknownError("Unknown type of item at " + position);
    }

    public void setReviews(List<Reviewer> reviews) {
        this.reviews = reviews;
    }



    abstract class ReviewerViewHolder<VB extends ViewDataBinding> extends RecyclerView.ViewHolder {
        final VB binding;

        public ReviewerViewHolder(VB binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ReviewerViewHolder(View view) {
            super(view);
            binding = null;
        }
    }


    class ComfortReviewViewHolder extends  ReviewerViewHolder<ComfortReviewItemViewBinding> {

        public ComfortReviewViewHolder(ComfortReviewItemViewBinding binding) {
           super(binding);
        }
    }

    class ResourceReviewViewHolder extends  ReviewerViewHolder<ResourceReviewItemViewBinding> {

        public ResourceReviewViewHolder(ResourceReviewItemViewBinding binding) {
            super(binding);
        }
    }
}
