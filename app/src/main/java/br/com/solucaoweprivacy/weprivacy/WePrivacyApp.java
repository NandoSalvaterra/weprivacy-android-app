package br.com.solucaoweprivacy.weprivacy;

import android.app.Application;

import br.com.solucaoweprivacy.weprivacy.client.WePrivacyService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by luizfernandosalvaterra on 27/02/2018.
 */

public class WePrivacyApp extends Application {

    public static WePrivacyApp instance;

    private WePrivacyService apiService;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(getString(R.string.weprivacy_api_url))
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiService  = retrofit.create(WePrivacyService.class);
    }

    public WePrivacyService getApiService() {
        return apiService;
    }



}
