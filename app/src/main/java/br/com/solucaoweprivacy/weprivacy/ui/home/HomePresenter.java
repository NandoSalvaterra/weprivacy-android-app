package br.com.solucaoweprivacy.weprivacy.ui.home;

import android.util.Log;

import java.util.List;

import br.com.solucaoweprivacy.weprivacy.client.WePrivacyService;
import br.com.solucaoweprivacy.weprivacy.model.App;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by luizfernandosalvaterra on 27/02/2018.
 */

public class HomePresenter implements  HomeContract.HomePresentation {

    private WePrivacyService apiService;
    private HomeContract.View homeView;

    public HomePresenter(WePrivacyService apiService, HomeContract.View homeView) {
        this.homeView = homeView;
        this.apiService = apiService;
    }

    @Override
    public void loadHomeApps(String id) {

        homeView.showIntermittentProgress(true);

        apiService.getHomeApps(id).enqueue(new Callback<List<App>>() {

            @Override
            public void onResponse(Call<List<App>> call, Response<List<App>> response) {
                homeView.showIntermittentProgress(false);
                homeView.showHomeApps(response.body());
            }

            @Override
            public void onFailure(Call<List<App>> call, Throwable t) {
                homeView.showIntermittentProgress(false);
                homeView.showError();
            }
        });

    }

    @Override
    public void refresh(String id) {

    }

    @Override
    public void onDestroy() {
        homeView = null;

    }
}
