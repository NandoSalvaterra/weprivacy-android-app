package br.com.solucaoweprivacy.weprivacy.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Comfort implements Parcelable{

    @SerializedName("idConforto")
    private String id;

    @SerializedName("descricao")
    private String description;

    @SerializedName("icone")
    private String icon;

    @SerializedName("indiceLikert")
    private double likertIndex;

    protected Comfort(Parcel in) {
        id = in.readString();
        description = in.readString();
        icon = in.readString();
        likertIndex = in.readDouble();
    }

    public static final Creator<Comfort> CREATOR = new Creator<Comfort>() {
        @Override
        public Comfort createFromParcel(Parcel in) {
            return new Comfort(in);
        }

        @Override
        public Comfort[] newArray(int size) {
            return new Comfort[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getLikertIndex() {
        return likertIndex;
    }

    public void setLikertIndex(double likertIndex) {
        this.likertIndex = likertIndex;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(description);
        parcel.writeString(icon);
        parcel.writeDouble(likertIndex);
    }
}
