package br.com.solucaoweprivacy.weprivacy.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ResourceReview implements Parcelable, Reviewer {

    @SerializedName("idAnaliseExpectativa")
    private String id;

    @SerializedName("percentual")
    private String average;

    @SerializedName("label")
   private String title;

    @SerializedName("icone")
    private String icon;

    @SerializedName("txtAnalise")
    private String description;

    private String finalDescription;


    protected ResourceReview(Parcel in) {
        id = in.readString();
        average = in.readString();
        title = in.readString();
        icon = in.readString();
        description = in.readString();
    }

    public static final Creator<ResourceReview> CREATOR = new Creator<ResourceReview>() {
        @Override
        public ResourceReview createFromParcel(Parcel in) {
            return new ResourceReview(in);
        }

        @Override
        public ResourceReview[] newArray(int size) {
            return new ResourceReview[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAverage() {
        return average;
    }

    public void setAverage(String average) {
        this.average = average;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFinalDescription() {
        return finalDescription;
    }

    public void setFinalDescription(String average, String description) {
        this.finalDescription = average + " " + description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(average);
        parcel.writeString(title);
        parcel.writeString(icon);
        parcel.writeString(description);
    }
}
