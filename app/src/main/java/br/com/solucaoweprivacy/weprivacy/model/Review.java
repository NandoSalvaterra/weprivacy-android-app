package br.com.solucaoweprivacy.weprivacy.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Review  implements Parcelable, Reviewer {

    @SerializedName("idAnaliseProposito")
    private String id;

    @SerializedName("percentual")
    private String average;

    @SerializedName("idConforto")
    private Comfort comfort;

    @SerializedName("icone")
    private String icon;

    @SerializedName("txtAnalise")
    private String description;

    private String finalDescription;

    protected Review(Parcel in) {
        id = in.readString();
        average = in.readString();
        comfort = in.readParcelable(Comfort.class.getClassLoader());
        icon = in.readString();
        description = in.readString();
        finalDescription = in.readString();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAvarage() {
        return average;
    }

    public void setAvarage(String avarage) {
        this.average = avarage;
    }

    public Comfort getComfort() {
        return comfort;
    }

    public void setComfort(Comfort comfort) {
        this.comfort = comfort;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFinalDescription() {
        return finalDescription;
    }

    public void setFinalDescription(String average, String description ) {
        this.finalDescription =  average + " " + description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(average);
        parcel.writeParcelable(comfort, i);
        parcel.writeString(icon);
        parcel.writeString(description);
        parcel.writeString(finalDescription);
    }
}
