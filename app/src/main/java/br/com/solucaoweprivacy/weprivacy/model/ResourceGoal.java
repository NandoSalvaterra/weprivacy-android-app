package br.com.solucaoweprivacy.weprivacy.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class ResourceGoal implements Parcelable {

    @SerializedName("idAppRecursoFinalidade")
    private String id;

    @SerializedName("idAplicativo")
    private App app;

    @SerializedName("idRecurso")
    private Resource resource;

    @SerializedName("idFinalidade")
    private Goal goal;

    protected ResourceGoal(Parcel in) {
        id = in.readString();
        app = in.readParcelable(App.class.getClassLoader());
        resource = in.readParcelable(Resource.class.getClassLoader());
        goal = in.readParcelable(Goal.class.getClassLoader());
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public App getApp() {
        return app;
    }

    public void setApp(App app) {
        this.app = app;
    }

    public Resource getResource() {
        return resource;
    }

    public void setResource(Resource resource) {
        this.resource = resource;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public static final Creator<ResourceGoal> CREATOR = new Creator<ResourceGoal>() {
        @Override
        public ResourceGoal createFromParcel(Parcel in) {
            return new ResourceGoal(in);
        }

        @Override
        public ResourceGoal[] newArray(int size) {
            return new ResourceGoal[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeParcelable(app, i);
        parcel.writeParcelable(resource, i);
        parcel.writeParcelable(goal, i);
    }
}
