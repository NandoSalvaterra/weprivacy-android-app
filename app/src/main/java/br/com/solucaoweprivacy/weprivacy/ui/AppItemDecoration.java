package br.com.solucaoweprivacy.weprivacy.ui;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.com.solucaoweprivacy.weprivacy.R;

/**
 * Created by Luiz Fernando Salvaterra on 27/09/2017.
 */

public class AppItemDecoration extends RecyclerView.ItemDecoration {

    private final int margin;
    private final int padding;

    public AppItemDecoration(Context context) {
        this.padding = context.getResources()
                .getDimensionPixelSize(R.dimen.space_card_mid);
        this.margin = context.getResources()
                .getDimensionPixelSize(R.dimen.space_card_top);


    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        GridLayoutManager gridLayoutManager = (GridLayoutManager) parent.getLayoutManager();

        final int pos = gridLayoutManager.getPosition(view);

        if (pos == 0 || pos == 1) {
            outRect.top = margin;
        } else {
            outRect.top = padding;
        }
        if(pos % 2 == 0) {
            outRect.left = margin;
            outRect.right = padding;
        } else {
            outRect.right = margin;
            outRect.left = padding;
        }

        outRect.bottom = padding;
    }
}
